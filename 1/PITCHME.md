---?image=1/assets/background/first-page.jpg

# Lessons from Elijah #

![The LORD vs Baal](1/assets/image/the-lord-vs-baal.jpg)

---?image=assets/background/background-1.jpg

## Getting Into Context ##

* Lets rewind to the year 874 BC (around 60 years after the reign of King Soloman) |
* Israel was split into the northern and southern kingdoms |
* Israel has had a series of very bad kings |
* The current king Ahab and queen Jezebel are no exception |
* Ahab was more evil than all the kings before him |
* They served and worshipped Baal and caused Israel to sin |

+++?image=1/assets/image/israel-after-split.png&size=auto 90%
@title[Map of Israel]

note:

* The capital of the Kingdom of Israel: Samaria |
* The capital of the Kingdom of Judah: Jerusalem |
* Also make note of Jezreel and Mount Carmel |

---

## Introducing Elijah ##

* Elijah is a prophet of the LORD |
* Elijah tells King Ahab that there will be no rain until God decides otherwise |
* God tells Elijah to flee so he will not be killed |

+++?image=1/assets/image/elijah-fed-by-ravens.jpg&size=auto 90%
@title[Image - Fed by ravens]
+++

## Fed by ravens (1 Kings 17:2-6) ##

* Raven's brought bread and meat in the mornings and evenings |
* Until the Cherith Brook dried up |

+++?image=1/assets/image/flour-oil-non-depleting.png&size=auto 90%
@title[Image - Oil and flour not depleting]
+++

## Oil and Flour Never Depleting (1 Kings 17:7-15) ##

* Elijah meets a poor widow and her son |
* She makes a loaf of bread for him with the last of her oil and flour |
* God blesses her by making the oil jar never deplete and flour to not decrease until it rains |

note:

The widow lived in Zarephath which is somewhere between Tyre and Sidon 

+++?image=1/assets/image/elijah-widow-boy.jpg&size=auto 90%
@title[Image - Raising widow's son from dead]
+++

## Raising the widows son from the dead. (1 Kings 17:17-24) ##

* The widows son becomes sick and dies |
* Elijah stretches himself over the boy three times and cries out to the LORD |
* The LORD returns the child to life |

+++?image=1/assets/image/baal-vs-elijah.jpg&size=auto 90%
@title[Image - Baal vs Elijah]
+++

## Baal vs The LORD (1 Kings 18-45) ##

### Elijah requests that the following people meet together ###

* The people of Israel |
* Jezebel's prophets of Baal and Asherah |
* To meet at Mount Carmel |

note:

* Asherah is the female counterpart of Baal

+++

### Elijah's Challenge to Israel is as follows ###

* If the LORD is God, follow Him; but if Baal is god, follow him |
* They have a contest with two sacrifices |
* The contest is to set the sacrifice on fire through prayer |
* The god who answers by fire — he is God |
* All of Israel agrees this this is a great idea |

+++

### The result ###

* The prophets of Baal cry out to their god all day and nothing happens |
* Elijah taunts them, they pray harder and still nothing happens |
* Elijah prepares the sacrifice on the LORD'S alter |
* He soaks the sacrifice and alter with water |
* Elijah prays and the LORDS’s fire falls |
* it consumed the sacrifice, the wood, the stones, the water and even the dust |
* Elijah commands the Israelites to kill Baal's prophets |

note:

* Remember to mention that Elijah repairs to LORDS alter
* He places 12 stones on the alter (one of each tribe)
* He pours 4 large jars of water over the alter 3 times
* Until its completely water logged

+++?image=1/assets/image/elijah-outrunning-chariot.jpg&size=auto 90%

@title[Image - Outrunning the King's Chariot on foot]

+++

## Outrunning the King's Chariot on foot (1 Kings 18:46) ##

* Rain is restored to the land of Israel after 3 years of no rain |
* Elijah is able to outrun King Ahab's chariot to all the way to Jezreel (around 20 km) |
* Jezebel threatens Elijah and he flees all the way to Beersheba in Judah |

---?image=1/assets/background/sad-background.jpg

## Elijah flees ##

1 Kings 19:3-5 NIV: Elijah was afraid and ran for his life. When he came to Beersheba in Judah, he left his servant there, while he himself went a day’s journey into the wilderness. He came to a broom bush, sat down under it and prayed that he might die. "I have had enough, LORD," he said. "Take my life; I am no better than my ancestors." Then he lay down under the bush and fell asleep.

+++?image=1/assets/background/sad-background.jpg

## Is this the end for Elijah? ##

* No |
* The LORD sends an angel to Elijah to wake him up and feed him and he travels for 40 days and 40 nights without rest |
* And later Elijah even meets the Lord in outside of a cave on mount Horeb which is also known as mount Sinai |

note:

* Mount Sinai is where God gave moses the Law (10 commandments)
* God doesn't reveal Himself in the strong wind, an earthquake or a raging fire but in a gentle whisper.

+++?image=1/assets/background/sad-background.jpg

![mount Horeb/Sinai](assets/image/mount-sinai-horeb.jpg)

+++?image=assets/background/sad-background.jpg

## His job wasn't done ##

* He needed to appoint two new kings (Jehu over Israel and Hazael over Aram) |
* He needed to train a new prophet for God: Elisha |
* And finally Elijah was taken up to heaven in a whirlwind (2 Kings 2-18) |

---?image=assets/background/gitpitch-audience.jpg

### Conclusion ###

* Just imagine what Elijah would have missed if he had given up |
* What is our response to Elijah? |
* How do we find ourselves in this story? |

note:

* We often find ourselves walking in God's power and authority until we get our figurative note from Jezabel
* Example notes could be:

    * A nasty remark on social media
    * A negative comment on the job you do in church
    * Anything really

* Then we end up running away from Church, from fellow believers.. and God comes through to bring us back and said... "My child, what are you doing here? Come back."