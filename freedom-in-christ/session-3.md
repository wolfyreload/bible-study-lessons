# The World's View Of The Truth #

## Objective ##

To understand that Christians need to make a definite decision to turn away from believing what the world teaches and instead believe what God says is true.

## Focus Verse ##

**Romans 12:2:**

Don't copy the behavior and customs of this world, but let God transform you into a new person by changing the way you think. Then you will learn to know God's will for you, which is good and pleasing and perfect.

## Focus Truth ##

The world we grew up in influenced us to view life in a particular way and to see this way as "true". However if it doesn't stack up to the way which God says is true, we need to reject it and bring our beliefs in line with with what is really true.

## Welcome ##

**If you could go anywhere in the world where would you choose?**

I guess I would go to Australia, I have family in Australia and it would be nice to visit them.

**Do you think that the way you look at the world and what you believe would be very different if you had been brought up in a different culture?**

* Yes

## Worship ##

The uniqueness of Jesus

**John 14:6:**

Jesus told him, “I am the way, the truth, and the life. No one can come to the Father except through me.

**Ephesians 1:16-23:**

I have not stopped thanking God for you. I pray for you constantly, asking God, the glorious Father of our Lord Jesus Christ, to give you spiritual wisdom and insight so that you might grow in your knowledge of God. I pray that your hearts will be flooded with light so that you can understand the confident hope he has given to those he called — his holy people who are his rich and glorious inheritance.

I also pray that you will understand the incredible greatness of God’s power for us who believe him. This is the same mighty power that raised Christ from the dead and seated him in the place of honor at God’s right hand in the heavenly realms. Now he is far above any ruler or authority or power or leader or anything else — not only in this world but also in the world to come. God has put all things under the authority of Christ and has made him head over all things for the benefit of the church. And the church is his body; it is made full and complete by Christ, who fills all things everywhere with himself.

**1 Corinthians 1:30:**

God has united you with Christ Jesus. For our benefit God made him to be wisdom itself. Christ made us right with God; he made us pure and holy, and he freed us from sin.

**Philippians 2:5-11:**

You must have the same attitude that Christ Jesus had.

Though he was God,
    he did not think of equality with God
    as something to cling to.
Instead, he gave up his divine privileges;
    he took the humble position of a slave
    and was born as a human being.
When he appeared in human form,
    he humbled himself in obedience to God
    and died a criminal’s death on a cross.

Therefore, God elevated him to the place of highest honor
    and gave him the name above all other names,
that at the name of Jesus every knee should bow,
    in heaven and on earth and under the earth,
and every tongue declare that Jesus Christ is Lord,
    to the glory of God the Father.

## Pause For Thought 1 ##

**What are the ways that the world made you feel insignificant, insecure and unloved?**

* During most of my high school days I had a nickname of "freak of nature". I was lead to believe that I was a looser and would never amount to anything. So insignificant, insecure and unloved? Yep. It took me many many years to get rid of this mind set and I took it with me into my early adult life.

**In what ways has the world promised you significance, security and acceptance?**

* The world promised me that I would be significant as I grow in my work career (significance)
* I've also been told that I could "make it" in the music world if I would if I would just drop the Christian stuff (acceptance)

**Do you recognize these false formulas?**

* **Performance + accomplishments = significance**
* **Status + recognition = security**
* **Appearance + admiration = acceptance**

* Yes and they overlap with each other e.g. (performance and recognition, status and accomplishments and admiration)

**How can you counteract "The lust of the flesh, the lust of the eyes and the pride of life" (1 John 2:15-17)**

* We need to acknowledge that the promises of the world are false. Pursuing the false formulas might actually make us feel great BUT those feelings do not last.
* We need to release that we cannot both love God and love the world at the same time, they are mutually exclusive.

## Pause For Thought 2 ##

**How different would your world view be if you grew up in a different part of the world or in a different time?**

* Very different. E.g. if I grew up in ancient Greece I would have believed in many gods and worshipped them.

**In what ways to you identify with one of the three worldviews discussed here?**

I identify most with the **Western World View** I used to completely ignore the spiritual world in its entirely. E.g. we only treat depression with medication and counselling and ignore the possibility that its a spiritual attack.

**When we talk to people about Jesus' and claim that he is the only way to God. How do we not come across as arrogant and intolerant?**

* We are doing a greater disservice toward people knowing the truth and not sharing it.
* Before telling someone about Jesus you need take the time to listen to them before babbling on about the Gospel
* However in the end, regardless of how you present the Gospel you are going to offend people that that is the reality of it.

## Witness ##

**How will understanding that everyone grows up according to a worldview help us to speak to people who are not yet Christians?**

* The more you understand about a person, the more you can relate to them. The more you can relate, the simpler it is to share Jesus with them.