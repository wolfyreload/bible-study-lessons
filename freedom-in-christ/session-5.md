# The Battle For Our Minds #

## Objective ##

To understand that although our enemy, the Devil, is constantly attempting to get us to believe lies, we don't have to believe every thought that comes into our heads. Instead we need to hold each one up against truth and choose to accept or reject it.

## Focus Verse ##

**Ephesians 6:11-18:**

Put on all of God’s armor so that you will be able to stand firm against all strategies of the devil. For we are not fighting against flesh-and-blood enemies, but against evil rulers and authorities of the unseen world, against mighty powers in this dark world, and against evil spirits in the heavenly places.

Therefore, put on every piece of God’s armor so you will be able to resist the enemy in the time of evil. Then after the battle you will still be standing firm. Stand your ground, putting on the belt of truth and the body armor of God’s righteousness. For shoes, put on the peace that comes from the Good News so that you will be fully prepared. In addition to all of these, hold up the shield of faith to stop the fiery arrows of the devil. Put on salvation as your helmet, and take the sword of the Spirit, which is the word of God.

Pray in the Spirit at all times and on every occasion. Stay alert and be persistent in your prayers for all believers everywhere.

## Focus Truth ##

We are all in a spiritual battle. It's a battle between truth and lies and it takes place in our minds. If we are aware of how Satan works, we not fall for his schemes.

## Welcome ##

**Has anyone ever played a really good trick on you (what was it)?**

**Have you ever played a really good trick on someone else (what was it)?**

## Worship ##

**Colossians 2:20-23**

 You have died with Christ, and he has set you free from the spiritual powers of this world. So why do you keep on following the rules of the world, such as, “Don’t handle! Don’t taste! Don’t touch!”? Such rules are mere human teachings about things that deteriorate as we use them. These rules may seem wise because they require strong devotion, pious self-denial, and severe bodily discipline. But they provide no help in conquering a person’s evil desires.

## Pause For Thought 1 ##

**Does the Devil seem more or less powerful than you imagined? In what ways**

* He actually seems more powerful that I thought as I didn't know that he could plant thoughts... but this makes sense as the Bible tells us to guard our minds.

**What do you think about this statement: "Not every thought that comes into your head is your own"?**

* Never looked at it before in this way.

**When you look back at your life can you identity occasions when a thought may have come from the enemy?**

* Multiple

**Are those thoughts always completely false?**

* They usually have an element of truth in them

## Pause For Thought 2 ##

**What are some of the ways that you might become aware of deception in your life? ("If you are deceived, by definition you don't know it")**

* If we take every thought captive and analyze it against the Truth of the bible then the deception will present itself.

**What practical steps can you take to "take every thought captive" to obey Christ. (2 Corinthians 10:5 We destroy every proud obstacle that keeps people from knowing God. We capture their rebellious thoughts and teach them to obey Christ.)**

* Journalling Perhaps? It can help us slow down and see deception for what it is?

**If I have given the enemy a foothold through sin, how can we take that foothold away according to James 4:7**

The verse is quite clear:

Step 1: Humble yourselves before God
Step 2: Resist the devil and he will flee from you.

## Reflection ##

**1 John 18b:**

The One who was born of God keeps them safe, and the evil one cannot harm them.

**Take some time to discuss this powerful truth and how it applied in our lives**

* This only applies if we are born again
* God will never let us go
* We are safe and secure in Him

## Witness ##

**How do you think that Satan works in the lives of your Non-Christian friends? What might you be able to do about this?**

* Satan works by blinding them to the Truth. If we were to share the gospel with them then they would no the truth and would be in a position to accept the truth rather than Satan's lies.