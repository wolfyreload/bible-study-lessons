# Who As I #

## Focus Verse ##

2 Corinthians 5:17 Anyone who belongs to Christ has become a new person. The old life is gone; a new life has begun!

## Objective ##

To realize that we are a new creation, even if it doesn't feel that way. We are "saints" or "holy-ones" who are accepted secure and significant.

## Welcome ##

The pairs are:

* Zander and Andrew
* Craig and Brenda
* Yvonne and Cobus
* Christa and Michael
* If Kathy is there I will sit this out and just co-ordinate

## Worship ##

God's plans and promises:

**Psalm 33:10-11:**

The LORD frustrates the plans of the nations and thwarts all their schemes.
But the LORD's plans stand firm forever; his intentions can never be shaken.

**Job 42:2:**

I know that you can do anything, and no one can stop You.

**Proverbs 21:11:**

You can make many plans, but the LORD's purpose will prevail.

## Pause For Thought 1 ##

**What prompted you to come to the Freedom in Christ course and what are your hopes for it?**

I am leading the Freedom in Christ course due to Brenda and Yvonne's recommendation. My initial hope was that it would require less of my personal time every week to prepare for Tuesday night Bible study :)

**What were the consequence of Adam and Eve's sin?**

* They were evicted from the garden of eden.
* An expiry date was added to their physical bodies.

**What are the ways it caused their relationship with God to change?**

* They were no longer connected to God's Spirit - they were now spiritually dead
* They no longer has close fellowship with God

**What things in our lives promise to make us feel accepted, significant and secure?**

* Our careers
* Money
* Stuff (material possessions)
* Ability
* Romance

## Pause For Thought 2 ##

**John 10:10 says that Jesus came to bring us life and life to the full. What do you think this might look like in practice.**

* The life that is spoken of starts immediately after coming to know Jesus is not speaking of when we die and go to heaven but NOW.
* It is a life where we can speak freely to God as a son speaking to a father.
* It is a life where we have the blessing of God over us. Although it doesn't feel this way when Calamity strikes.

**How does knowing that we are "Saints" or "Holy-Ones" change the way that we see ourselves?**

* I have always viewed myself as a sinner who is saved. Changing the perspective to being a saint who sometimes sins changes the way I see myself.
* Viewing myself as a sinner who is saved, made me think God can never fully accept me as I am until I die and go to heaven.
* But viewing myself as a saint who sometimes sins, helps me to understand that the kingdom on God is here and now.

**What are some of the things that could prevent us from fully knowing that we are now new creations in Christ that are completely forgiven?**

* If we are stuck in habitual sin.
* If we are not making an attempt to read our Bibles to know God more.
* Constant negative thoughts and the deceitful lies of Satan

## Witness ##

* A Christian is someone who follows the son of God, the Lord Jesus Christ. By following, they attempt to become like Christ.
* A non-Christian is a man or woman who does none of the above.
* It is better to be a Christian because that is where true life actually starts.
* Life without Christ has pretty much no hope, feels pointless, etc.