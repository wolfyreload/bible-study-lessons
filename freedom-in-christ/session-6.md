# Handling Emotions Well #

## Objective ##

To understand our emotions nature and how it is related to what we believe.

## Focus Verse ##

**1 Peter 5:7-8:**

Give all your worries and cares to God, for he cares about you.

Stay alert! Watch out for your great enemy, the devil. He prowls around like a roaring lion, looking for someone to devour.

## Focus Truth ##

Our emotions are essentially a product of our thoughts and a barometer of our spiritual health.

## Welcome ##

## Worship ##

## Pause For Thought 1 ##

**Matthew 26:37:** He took Peter and Zebedee’s two sons, James and John, and he became anguished and distressed. (This is just before Jesus is arrested)

**2 Samuel 6:14:** And David danced before the Lord with all his might, wearing a priestly garment. (after bringing back the Ark of the LORD)

**What do these verses teach us?**

Our emotions can serve as a gauge of our spiritual health. But I'm not sure that these are the two best examples as Jesus had the best spiritual health at all times?

**God gave us the ability to feel emotional pain for our own protection. Do you agree?**

Yes. Just like feeling pain when a child touches a hot stove plate.

**How do you think that works in practice?**

E.g. you in a terrible mood and a friend tries to make a joke and you have a full sense of humor failure and tear him emotionally to shreds. You going to feel terrible about what you have done which will hopefully lead to asking for forgiveness and praying for more self control.

**Discuss the statement: "If what you believe does not reflect what is actually true, then what you feel won't reflect reality"**

* e.g. If you believe that you are a disappointment to everyone. You will most likely feel miserable all the time and that life is completely pointless. But the reality is that you are not a disappointment to everyone.

## Pause For Thought 2 ##

**Describe a life-goal you have had that you thought would make you feel significant, secure, and accepted that ended up being "blocked"**

* One of my life goals was to meet someone and be married by the age of 25. I seem to keep missing the deadlines on this one....

**How can traumatic experiences lead us to believe a lie about ourselves, God or Satan**

* I was bullied all through school and was called a "freak of nature". Even after becoming a Christian is was many many years that I believed that was my identity.

**Discuss this statement: "Children of God are not primarily products of their past. They are primarily products of Christ's work on the cross and His resurrection. Nobody can change our past, bu we can choose to walk free of it. That's the whole point of the Gospel"**

* We cannot change the past so there is no purpose on constantly dwelling on the past.
* If we change our focus to Jesus and His purpose for our lives, we can walk free of the past.

## Reflection ##

**How easy do you find it to tell God exactly how you feel?**

I struggle to put into words exactly how I feel most of the time. But when I can put words to it I don't find it difficult to speak to God.

**How does understanding the truth about Him make it easier to be emotionally honest with Him?**

Since I know that God understands EXACTLY how I feel. It makes it a lot easier to be honest with Him.