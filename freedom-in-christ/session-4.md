# Our Daily Choice #

## Objective ##

To understand that we still have urges that tend to pull us away from relying completely on God and following the promptings of His Spirit. We no longer have to give into the urges but are free to make genuine choice.

## Focus Verse ##

**Romans 8:9a:**

But you are not controlled by your sinful nature. You are controlled by the Spirit if you have the Spirit of God living in you. (And remember that those who do not have the Spirit of Christ living in them do not belong to him at all.)

## Focus Truth ##

Although you are a new person (creation) in Christ with a completely new nature, you are free to live according to what the Holy Spirit tells you. However obeying Him is not automatic.

## Welcome ##

**What would you most likely do if you knew that you could not fail?**

I would probably be in music ministry full time.

## Worship ##

Worship Him for who He is.

**Hebrews 13:15:**

Therefore, let us offer through Jesus a continual sacrifice of praise to God, proclaiming our allegiance to his name.

**Revelation 19:5:**

And from the throne came a voice that said,

“Praise our God,
    all his servants,
all who fear him,
    from the least to the greatest.”

**Psalm 99:9:**

Exalt the Lord our God,
    and worship at his holy mountain in Jerusalem,
    for the Lord our God is holy!

**1 Chronicles 29:11-13:**

Yours, O Lord, is the greatness, the power, the glory, the victory, and the majesty. Everything in the heavens and on earth is yours, O Lord, and this is your kingdom. We adore you as the one who is over all things. Wealth and honor come from you alone, for you rule over everything. Power and might are in your hand, and at your discretion people are made great and given strength.

O our God, we thank you and praise your glorious name!

## Pause For Thought 1 ##

**What are some of the main things that you notice have changed in you since you became a Christian?**

* I'm going to share things that have changed as I have grown in Christ as I cannot remember much of my life before being a Christian. I have more confidence in my walk with God.

**And what are some of the things that you wish had changed but haven't?**

* I wish that my ability to interact with people was better.

**In what particular type of situation do you recognize that you become more vulnerable to the attempts of the flesh to draw you towards sin?**

**What practical steps can you put in place to help you at these times?**

**God's Word tells us that we are alive to Christ and dead to sin. Why does this not feel true some days? How can we "rise above the law of sin"?**

* We need to remember that we are free to turn away from sin however impossible it feels to do.

## Pause For Thought 2 ##

**How do you identify with the descriptions of the three different types of people that are mentioned in 1 Corinthians 2:14-3:3?**

* I identity with the fleshy person more than the spiritual person.

**How have the barriers for growth mentioned caused you to show more of the characteristics of the "fleshy person" than a "spiritual person"?**

* In my case its always been deception. e.g. "God could never really use me"

**What practical steps and daily choices can we put into place to ensure that we act as a spiritual person?**

* We need to break away from the mental deceptions that we bring with us after becoming a Christian
* We need to forgive fully and wholeheartedly.
* We need to take responsibility of our own spiritual growth and not blame our lack of growth on God or others.

## Reflection ##

**Who was responsible for "fanning into flame" the gift of the Holy Spirit in Timothy's life? Was it God, Paul or Timothy?**

* It was Timothy's responsibility

**Whose responsibility is it to do that in your life?**

* 100% my responsibility

**What are some of the ways you could do that?**

* By walking in the Spirit

**Take some time on your own in prayer to commit to walk by the Spirit rather than the flesh. Fan the gift of the Holy Spirit into flame in your life.**

## Witness ##

**How would you explain to an unbeliever the benefits of being filled with the Holy Spirit that would make sense to them?**