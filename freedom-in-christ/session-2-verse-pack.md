# Choosing To Believe The Truth #

## Focus Verse ##

Hebrews 11:6 And it is impossible to please God without faith. Anyone who wants to come to him must believe that God exists and that he rewards those who sincerely seek him.

## Welcome ##

## Worship ##

Release just how much God loves us and delights in us.

**Ephesians 3:14-20:** (Paul's Prayer for Spiritual Growth)

 When I think of all this, I fall to my knees and pray to the Father, the Creator of everything in heaven and on earth. I pray that from his glorious, unlimited resources he will empower you with inner strength through his Spirit. Then Christ will make his home in your hearts as you trust in him. Your roots will grow down into God's love and keep you strong. And may you have the power to understand, as all God's people should, how wide, how long, how high, and how deep his love is. May you experience the love of Christ, though it is too great to understand fully. Then you will be made complete with all the fullness of life and power that comes from God.

 Now all glory to God, who is able, through his mighty power at work within us, to accomplish infinitely more than we might ask or think. Glory to him in the church and in Christ Jesus through all generations forever and ever! Amen.

**Skip Zephaniah 3:17 (not certain of the context)**

**2 Corinthians  3:16-18:** (Note: the veil is a hardened heart covering our minds)

But whenever someone turns to the Lord, the veil is taken away. For the Lord is the Spirit, and wherever the Spirit of the Lord is, there is freedom. So all of us who have had that veil removed can see and reflect the glory of the Lord. And the Lord — who is the Spirit — makes us more and more like him as we are changed into his glorious image.

**Hebrews 12:1-2:**

Therefore, since we are surrounded by such a huge crowd of witnesses to the life of faith, let us strip off every weight that slows us down, especially the sin that so easily trips us up. And let us run with endurance the race God has set before us. We do this by keeping our eyes on Jesus, the champion who initiates and perfects our faith. Because of the joy awaiting him, he endured the cross, disregarding its shame. Now he is seated in the place of honor beside God's throne.

**Psalm 103:8-18:**

The Lord is compassionate and merciful,
    slow to get angry and filled with unfailing love.
He will not constantly accuse us,
    nor remain angry forever.
He does not punish us for all our sins;
    he does not deal harshly with us, as we deserve.
For his unfailing love toward those who fear him
    is as great as the height of the heavens above the earth.
He has removed our sins as far from us
    as the east is from the west.
The Lord is like a father to his children,
    tender and compassionate to those who fear him.
For he knows how weak we are;
    he remembers we are only dust.
Our days on earth are like grass;
    like wildflowers, we bloom and die.
The wind blows, and we are gone —
    as though we had never been here.
But the love of the Lord remains forever
    with those who fear him.
His salvation extends to the children's children
    of those who are faithful to his covenant,
    of those who obey his commandments!

## Witness ##

**2 Corinthians 4:4:**

Satan, who is the god of this world, has blinded the minds of those who don't believe. They are unable to see the glorious light of the Good News. They don't understand this message about the glory of Christ, who is the exact likeness of God.

**Romans 10:14-15:**

But how can they call on him to save them unless they believe in him? And how can they believe in him if they have never heard about him? And how can they hear about him unless someone tells them? 15 And how will anyone go and tell them without being sent? That is why the Scriptures say, "How beautiful are the feet of messengers who bring good news!"

## Twenty Cans Of Success ##

1. Why should I say I can't when the Bible says I can do all things through Christ who gives me strength?

    **Philippians 4:13**

    For I can do everything through Christ, who gives me strength.

1. Why should I lack when I know that God shall supply all my needs according to His riches in glory in Christ Jesus?

    **Philippians 4:19**

    And this same God who takes care of me will supply all your needs from his glorious riches, which have been given to us in Christ Jesus.

1. Why should I fear when the Bible says God has not given me a spirit of fear, but of power, love and a sound mind?

    **2 Timothy 1:7**

    For God has not given us a spirit of fear and timidity, but of power, love, and self-discipline.

1. Why should I lack faith to fulfill my calling knowing that God has allotted to me a measure of faith?

    **Romans 12:3**

    Because of the privilege and authority God has given me, I give each of you this warning: Don't think you are better than you really are. Be honest in your evaluation of yourselves, measuring yourselves by the faith God has given us.

1. Why should I be weak when the Bible says that the LORD is the strength of my life and that I will display strength and take action because I know God.

    **Psalm 27:1**

    The Lord is my light and my salvation —
        so why should I be afraid?
    The Lord is my fortress, protecting me from danger,
        so why should I tremble?

1. Why should I allow Satan supremacy over my life when He that is in me is greater than he that is in the world?

    **1 John 4:4**

    But you belong to God, my dear children. You have already won a victory over those people, because the Spirit who lives in you is greater than the spirit who lives in the world.

1. Why should I accept defeat when the Bible says that God always leads me in triumph

    **2 Corinthians 2:14**

    But thank God! He has made us his captives and continues to lead us along in Christ's triumphal procession. Now he uses us to spread the knowledge of Christ everywhere, like a sweet perfume.

1. Why should I lack wisdom when Christ became wisdom to me from God and God gives wisdom to me generously when I ask Him for it?

    **1 Corinthians 1:30**

    God has united you with Christ Jesus. For our benefit God made him to be wisdom itself. Christ made us right with God; he made us pure and holy, and he freed us from sin.

    **James 1:5**

    If you need wisdom, ask our generous God, and he will give it to you. He will not rebuke you for asking.

1. Why should I be depressed when I can recall to mind God's loving kindness, compassion and faithfulness and have hope?

    **Lamentations 3:21-23**

    Yet I still dare to hope when I remember this:

    * The faithful love of the Lord never ends!
    * His mercies never cease.
    * Great is his faithfulness;
    * His mercies begin afresh each morning.

1. Why should I worry and fret when I can cast all my anxiety on Christ who cared for me?

    **1 Peter 5:7**

    Give all your worries and cares to God, for he cares about you.

1. Why should I ever be in bondage knowing that, where the Spirit of the Lord is, there is freedom?

    **2 Corinthians 3:17**

    For the Lord is the Spirit, and wherever the Spirit of the Lord is, there is freedom.

    **Galatians 5:1**

    So Christ has truly set us free. Now make sure that you stay free, and don't get tied up again in slavery to the law.

1. Why should I feel condemned when the Bible says I am not condemned because I am in Christ?

    **Romans 8:1**

    So now there is no condemnation for those who belong to Christ Jesus.

1. Why should I feel alone when Jesus said He is with me always and He will never leave me nor forsake me?

    **Matthew 28:20**

    Teach these new disciples to obey all the commands I have given you. And be sure of this: I am with you always, even to the end of the age.

    **Hebrews 13:5**

    Don't love money; be satisfied with what you have. For God has said,

    "I will never fail you.
    I will never abandon you."

1. Why should I feel accursed or that I am the victim of bad luck when the Bible says that Christ redeemed me from the curse of the law that I might receive His Spirit?

    **Galatians 3:13-14**

    But Christ has rescued us from the curse pronounced by the law. When he was hung on the cross, he took upon himself the curse for our wrongdoing. For it is written in the Scriptures, "Cursed is everyone who is hung on a tree." Through Christ Jesus, God has blessed the Gentiles with the same blessing he promised to Abraham, so that we who are believers might receive the promised Holy Spirit through faith.

1. Why should I be discontented when I, like Paul, can learn to be content in all my circumstances?

    **Philippians 4:11**

    Not that I was ever in need, for I have learned how to be content with whatever I have.

1. Why should I feel worthless when Christ became sin on my behalf that I might become the righteousness of God in Him

    **2 Corinthians 5:21**

    For God made Christ, who never sinned, to be the offering for our sin, so that we could be made right with God through Christ.

1. Why should I have a persecution complex knowing that nobody can be against me when God is for me

    **Romans 8:31**

    What shall we say about such wonderful things as these? If God is for us, who can ever be against us?

1. Why should I be confused when God is the author of peace and He gives me knowledge through his indwelling Spirit.

    **1 Corinthians 14:33**

    For God is not a God of disorder but of peace, as in all the meetings of God's holy people.

    **1 Corinthians 2:12**

    And we have received God's Spirit (not the world's spirit), so we can know the wonderful things God has freely given us.

1. Why should I feel like a failure when I am a conqueror in all things through Christ?

    **Romans 8:37**

    No, despite all these things, overwhelming victory is ours through Christ, who loved us.

1. Why should I let the pressures of life bother me when I can take courage knowing that Jesus has overcome the world and its tribulations

    **John 16:33**

    I have told you all this so that you may have peace in me. Here on earth you will have many trials and sorrows. But take heart, because I have overcome the world.