# Who As I #

## Focus Verse ##

**2 Corinthians 5:17** Anyone who belongs to Christ has become a new person. The old life is gone; a new life has begun!

## Worship ##

God's plans and promises:

**Psalm 33:10-11:**

The LORD frustrates the plans of the nations and thwarts all their schemes.
But the LORD's plans stand firm forever; his intentions can never be shaken.

**Job 42:2:**

I know that you can do anything, and no one can stop You.

**Proverbs 19:21:**

You can make many plans, but the LORD's purpose will prevail.

# Who Am I Lists #

## I Am Accepted ##

**John 1:12** (I am God's Child):

But to all who believed him and accepted him, he gave the right to become children of God.

**John 15:15** (NOT John 15:5) (I am Christ's friend):

I no longer call you slaves, because a master doesn't confide in his slaves. Now you are my friends, since I have told you everything the Father told me.

**Romans 5:1** (I have been justified):

Therefore, since we have been made right in God's sight by faith, we have peace with God because of what Jesus Christ our Lord has done for us.

**1 Corinthians 6:17** (I am united with the Lord and I am one spirit with him):

The person who is joined to the Lord is one spirit with him.

**1 Corinthians 6:19-20** (I have been bought with a price and I belong to God):

Don't you realize that your body is the temple of the Holy Spirit, who lives in you and was given to you by God? You do not belong to yourself, for God bought you with a high price. So you must honor God with your body.

**1 Corinthians 12:27** (I am a member of the body of Christ):

All of you together are Christ's body, and each of you is a part of it.

**Ephesians 1:1** (I am a saint/holy-one):

This letter is from Paul, chosen by the will of God to be an apostle of Christ Jesus.

I am writing to God's **holy people** in Ephesus, **who are faithful followers of Christ Jesus**

**Ephesians 1:5** (We are adopted into God's family):

God decided in advance to adopt us into his own family by bringing us to himself through Jesus Christ. This is what he wanted to do, and it gave him great pleasure.

**Ephesians 2:18** (We have direct access to God through the Holy Spirit):

Now all of us can come to the Father through the same Holy Spirit because of what Christ has done for us.

**Colossians 1:13-14** (We are redeemed and our sins are forgiven):

For he has rescued us from the kingdom of darkness and transferred us into the Kingdom of his dear Son, who purchased our freedom and forgave our sins.

**Colossians 2:10** (You are complete in Christ):

So you also are complete through your union with Christ, who is the head over every ruler and authority.

## I Am Secure ##

**Romans 8:1-2** (I am free from condemnation):

So now there is no condemnation for those who belong to Christ Jesus. And because you belong to him, the power of the life-giving Spirit has freed you from the power of sin that leads to death.

**Romans 8:28** (I am assured that all thinks work together for good):

And we know that God causes everything to work together for the good of those who love God and are called according to his purpose for them.

**Romans 8:31-34** (I am free from any condemning charges against me):

What shall we say about such wonderful things as these? If God is for us, who can ever be against us? Since he did not spare even his own Son but gave him up for us all, won't he also give us everything else? Who dares accuse us whom God has chosen for his own? No one — for God himself has given us right standing with himself. Who then will condemn us? No one — for Christ Jesus died for us and was raised to life for us, and he is sitting in the place of honor at God's right hand, pleading for us.

**Romans 8:35-39** (I cannot be separated from the Love of God):

Can anything ever separate us from Christ's love? Does it mean he no longer loves us if we have trouble or calamity, or are persecuted, or hungry, or destitute, or in danger, or threatened with death? (As the Scriptures say, "For your sake we are killed every day; we are being slaughtered like sheep." No, despite all these things, overwhelming victory is ours through Christ, who loved us.

And I am convinced that nothing can ever separate us from God's love. Neither death nor life, neither angels nor demons, neither our fears for today nor our worries about tomorrow — not even the powers of hell can separate us from God's love. No power in the sky above or in the earth below — indeed, nothing in all creation will ever be able to separate us from the love of God that is revealed in Christ Jesus our Lord.

**2 Corinthians 1:21-22** (I have been established, anointed and sealed by God):

It is God who enables us, along with you, to stand firm for Christ. He has commissioned us, and he has identified us as his own by placing the Holy Spirit in our hearts as the first installment that guarantees everything he has promised us.

**Philippians 1:6** (I am confident that the good work God has begun in me will be completed):

And I am certain that God, who began the good work within you, will continue his work until it is finally finished on the day when Christ Jesus returns.

**Philippians 3:20** (We are citizen's of heaven):

But we are citizens of heaven, where the Lord Jesus Christ lives. And we are eagerly waiting for him to return as our Savior.

**Colossians 3:3** (I am hidden with Christ in God)

For you died to this life, and your real life is hidden with Christ in God.

**2 Timothy 1:7** (I have not been given a spirit of fear, but of power, love and self-control):

For God has not given us a spirit of fear and timidity, but of power, love, and self-discipline.

**Hebrews 4:16** (I can find grace and mercy to help in time of need):

So let us come boldly to the throne of our gracious God. There we will receive his mercy, and we will find grace to help us when we need it most.

**1 John 5:18** (I am born of God and the evil one cannot touch me)

We know that God's children do not make a practice of sinning, for God's Son holds them securely, and the evil one cannot touch them.

## I Am Significant ##

**Matthew 5:13-16** (I am the salt of the earth and the light of the world):

"You are the salt of the earth. But what good is salt if it has lost its flavor? Can you make it salty again? It will be thrown out and trampled underfoot as worthless.

"You are the light of the world — like a city on a hilltop that cannot be hidden. No one lights a lamp and then puts it under a basket. Instead, a lamp is placed on a stand, where it gives light to everyone in the house. In the same way, let your good deeds shine out for all to see, so that everyone will praise your heavenly Father.

**John 15:1-5** (I am a branch of the true vine, Jesus. Our lives are a channel of Christ's life):

"I am the true grapevine, and my Father is the gardener. He cuts off every branch of mine that doesn't produce fruit, and he prunes the branches that do bear fruit so they will produce even more. You have already been pruned and purified by the message I have given you. Remain in me, and I will remain in you. For a branch cannot produce fruit if it is severed from the vine, and you cannot be fruitful unless you remain in me.

"Yes, I am the vine; you are the branches. Those who remain in me, and I in them, will produce much fruit. For apart from me you can do nothing.

**John 15:1-5** (I have been chosen and appointed by God to bear fruit):

You didn't choose me. I chose you. I appointed you to go and produce lasting fruit, so that the Father will give you whatever you ask for, using my name.

**Acts 1:8** (I am a personal, Spirit-empowered witness of Christ's):

But you will receive power when the Holy Spirit comes upon you. And you will be my witnesses, telling people about me everywhere — in Jerusalem, throughout Judea, in Samaria, and to the ends of the earth.

**1 Corinthians 3:16** (We are the temple of God):

Don't you realize that all of you together are the temple of God and that the Spirit of God lives in you?

**2 Corinthians 5:17-21** (I am a ministers of reconciliation and Christ's ambassadors)

This means that anyone who belongs to Christ has become a new person. The old life is gone; a new life has begun!

And all of this is a gift from God, who brought us back to himself through Christ. And God has given us this task of reconciling people to him. For God was in Christ, reconciling the world to himself, no longer counting people's sins against them. And he gave us this wonderful message of reconciliation. So we are Christ's ambassadors; God is making his appeal through us. We speak for Christ when we plead, "Come back to God!" For God made Christ, who never sinned, to be the offering for our sin, so that we could be made right with God through Christ.

**2 Corinthians 6:1** (I am a fellow worker with God):

As God's partners, we beg you not to accept this marvelous gift of God's kindness and then ignore it.

**Ephesians 2:6** (I am seated with Christ in the heavenly realms):

For he raised us from the dead along with Christ and seated us with him in the heavenly realms because we are united with Christ Jesus.

**Ephesians 2:10** (I am God's workmanship, created for good works):

For we are God's masterpiece. He has created us anew in Christ Jesus, so we can do the good things he planned for us long ago.

**Ephesians 3:12** (I may approach God with freedom and confidence):

Because of Christ and our faith in him, we can now come boldly and confidently into God's presence.

**Philippians 4:13** (I can do all things through Christ who strengthens me):

For I can do everything through Christ, who gives me strength.

## Closing Statement ##

I am not the great "I Am," but by the grace of God I am what I am.

**Exodus 3:14:**

God replied to Moses, "I AM WHO I AM. Say this to the people of Israel: I AM has sent me to you."

**John 8:23-24:**

Jesus continued, "You are from below; I am from above. You belong to this world; I do not. That is why I said that you will die in your sins; for unless you believe that I am who I claim to be, you will die in your sins."

**John 8:28:**

So Jesus said, "When you have lifted up the Son of Man on the cross, then you will understand that I am he. I do nothing on my own but say only what the Father taught me. And the one who sent me is with me — he has not deserted me. For I always do what pleases him."

**John 8:57-58:**

The people said, "You aren’t even fifty years old. How can you say you have seen Abraham?"

Jesus answered, "I tell you the truth, before Abraham was even born, I AM!"

**1 Corinthians 15:10-11:**

But whatever I am now, it is all because God poured out his special favor on me — and not without results. For I have worked harder than any of the other apostles; yet it was not I but God who was working through me by his grace. So it makes no difference whether I preach or they preach, for we all preach the same message you have already believed.