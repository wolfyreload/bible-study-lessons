# Choosing To Believe The Truth #

## Focus Verse ##

Hebrews 11:6 And it is impossible to please God without faith. Anyone who wants to come to him must believe that God exists and that he rewards those who sincerely seek him.

## Objective ##

To understand that everyone lives by faith in something or someone and that faith in God is no more than finding out what is already actually true and choosing to believe and act on it.

## Focus Truth ##

God is truth. Find out what He has said is true and choose to believe it, whether it feels true or not, and your Christian life will be transformed.

## Welcome ##

**Have you had a prayer answered recently?**

* Yes, God has been helping me to be content what who I am, where I am and helping me to not overly stress over what is to come.

**Do you believe that an atheist has more or less faith than a Christian?**

* I believe that an atheist has more faith, as it requires a lot more faith to believe that everything came into being by chance than acknowledging the hand of a Creator God.

**What about a Hindu or a Muslim?**

* About the same as a Christian.

**What about someone who "just doesn't know?"**

* Even someone who "just doesn't know" has faith in something, it might be in their family, their friends etc... They just don't have their faith in God or some other religion.

## Worship ##

Release just how much God loves us and delights in us.

**Ephesians 3:14-20:** (Paul's Prayer for Spiritual Growth)

 When I think of all this, I fall to my knees and pray to the Father, the Creator of everything in heaven and on earth. I pray that from his glorious, unlimited resources he will empower you with inner strength through his Spirit. Then Christ will make his home in your hearts as you trust in him. Your roots will grow down into God’s love and keep you strong. And may you have the power to understand, as all God’s people should, how wide, how long, how high, and how deep his love is. May you experience the love of Christ, though it is too great to understand fully. Then you will be made complete with all the fullness of life and power that comes from God.

 Now all glory to God, who is able, through his mighty power at work within us, to accomplish infinitely more than we might ask or think. Glory to him in the church and in Christ Jesus through all generations forever and ever! Amen.

**Skip Zephaniah 3:17 (not certain of the context)**

**2 Corinthians  3:16-18:** (Note: the veil is a hardened heart covering our minds)

But whenever someone turns to the Lord, the veil is taken away. For the Lord is the Spirit, and wherever the Spirit of the Lord is, there is freedom. So all of us who have had that veil removed can see and reflect the glory of the Lord. And the Lord — who is the Spirit — makes us more and more like him as we are changed into his glorious image.

**Hebrews 12:1-2:**

Therefore, since we are surrounded by such a huge crowd of witnesses to the life of faith, let us strip off every weight that slows us down, especially the sin that so easily trips us up. And let us run with endurance the race God has set before us. We do this by keeping our eyes on Jesus, the champion who initiates and perfects our faith. Because of the joy awaiting him, he endured the cross, disregarding its shame. Now he is seated in the place of honor beside God’s throne.

**Psalm 103:8-17:** (To read in your free time)

## Pause For Thought 1 ##

**If you want to know what someone really believes, don't listen to what they say, but look at what they actually do.** Discuss.

**God asks us to believe what the Bible says is true, even when is might not feel true. Share about a time when you chose to override your feelings and acted by faith instead.**

A time that comes to minds would be the first time I played guitar in church. I didn't feel like I was getting anywhere with the instrument. But I had faith that God would use me in worship.

**It takes more faith to believe that there is no God than to believe in God. What do you think about this statement?**

* Agree fully.
* Especially when it comes to the story of creation.

## Pause For Thought 2 ##

**What are some of the ways our faith grows?**

* Our faith grows as our trust grows in God.
* The more of our lives that we hand over to God, the more we trust in God, the more our faith grows.

**In what circumstances or seasons in your life have you noticed your faith grow the most?**

* During trials or wilderness experiences when my faith is under pressure is when I notice faith growing the most.
* **Romans 5:3-5** We can rejoice, too, when we run into problems and trials, for we know that they help us develop endurance. And endurance develops strength of character, and character strengthens our confident hope of salvation. And this hope will not lead to disappointment. For we know how dearly God loves us, because he has given us the Holy Spirit to fill our hearts with his love.

**What are some of the ways you have discovered that help you get to know God better?**

* Bible reading and prayer

## Witness ##

**2 Corinthians 4:4:**

Satan, who is the god of this world, has blinded the minds of those who don’t believe. They are unable to see the glorious light of the Good News. They don’t understand this message about the glory of Christ, who is the exact likeness of God.

**Romans 10:14-15:**

But how can they call on him to save them unless they believe in him? And how can they believe in him if they have never heard about him? And how can they hear about him unless someone tells them? 15 And how will anyone go and tell them without being sent? That is why the Scriptures say, "How beautiful are the feet of messengers who bring good news!"

**Think of a Non-Christian friend. What does the Bible say about why they don't believe?**

They don't believe because the truth is hidden from them because their minds have been blinded by Satan. Secondly they do not believe because no-one has told them about Jesus.

**Write a prayer for said person and pray the prayer**

Dear LORD Jesus. I bring my friend Andrew Burgess before you LORD. He doesn't know you and I pray that He may come to know You. If I am the vessel that you wish to use for this purpose. Use me and equip me in this task. Amen.
