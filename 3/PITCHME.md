---?image=3/assets/background/first-page.jpg

@title[Out of Context]

![out of context](3/assets/image/out-of-context.png)

---

## What Is Context? ##

* The circumstances that form the setting for an event, statement, or idea, and in terms of which it can be fully understood. |
* Imagine opening the book, turn to a specific page and read one sentence. |
* Now write a 2 page book report on what the book is about. |
* Impossible? agreed it's impossible. So why do we do this when we read our Bibles? |

+++

## Bible Context ##

* Context is the key to good Biblical interpretation. |
* It's helpful to think of the Bible as a series of letters written between different people that we are to read and learn from. |
* Not ALL promises from God that we read in scripture have anything to do with us |

---

# Jeremiah 29:11 #

"For I know the plans I have for you," declares the Lord, "plans to prosper you and not to harm you, plans to give you hope and a future."

note:

* Probably the most misquoted verse in the Bible
* We quote this scripture to people who have lost love ones.
* To the terminally ill
* To anyone that is suffering typically
* Sometimes its also used by those who follow the properity gospel (becuase of the word prosper)

+++

## Putting Jeremiah 29:11 Into Context ##

* Hananiah, a false prophet, tells the exiles that God is going to free Judah from Babylon in two years. |
* Spoiler alert: God doesn’t do this |
* Jeremiah, the true prophet of God is writing to the exiles in Judah in Babylon |
* They will only be rescued after 70 years |
* God's plans in this verse are purely for those exiled in Babylon |

---

# Matthew 18:20 #

"For where two or three are gathered in my name, there am I among them."

note:

* This verse is used a lot, I have mis-used this verse a number of times until I knew its context
* You will often hear it used in the prayer room before a service.

+++

## Putting Matthew 18:20 Into Context ##

* This verse is about how to deal with sin in the Church |
* Read Matthew 18:15-18 |
* Note: even if only one person is gathered God is still with us. |

---?image=assets/background/gitpitch-audience.jpg

### Conclusion ###

* Always read the full chapter or at least the paragraph where the verse is from |
* Is also helps to know who the author of the verse is writing to |
* Can you think of a verse that is often taken out of context? |