# Key Texts #

**Genesis 35:16-20**

```bible
16 Leaving Bethel, Jacob and his clan moved on toward Ephrath. But Rachel went into labor while they were still some distance away. Her labor pains were intense. 17 After a very hard delivery, the midwife finally exclaimed, "Don't be afraid—you have another son!" 18 Rachel was about to die, but with her last breath she named the baby Ben-oni (which means "son of my sorrow"). The baby's father, however, called him Benjamin (which means "son of my right hand"). 19 So Rachel died and was buried on the way to Ephrath (that is, Bethlehem). 20 Jacob set up a stone monument over Rachel's grave, and it can be seen there to this day.
```

**Jeremiah 31:15**

```bible
15 This is what the Lord says:

"A cry is heard in Ramah—
    deep anguish and bitter weeping.
Rachel weeps for her children,
    refusing to be comforted—
    for her children are gone."
```

**Matthew 2:16-18**

16 Herod was furious when he realized that the wise men had outwitted him. He sent soldiers to kill all the boys in and around Bethlehem who were two years old and under, based on the wise men's report of the star's first appearance. 17 Herod's brutal action fulfilled what God had spoken through the prophet Jeremiah:

18 "A cry was heard in Ramah—
    weeping and great mourning.
Rachel weeps for her children,
    refusing to be comforted,
    for they are dead."

[Bethel to Bethlehem](bethel-to-bethlehem.jpg)

Distance between Ramah and Bethlehem is around 18KM

# Bible Threading #

In Genesis, Rachel dies giving birth while on the road to Bethlehem. In the midst of her suffering, the midwife tries to comfort her with the news that she is having another son. In this way, her child is both her cause of weeping and her hope for the future. (remember she believes that Joseph is dead at this point of time, so to her Benjamin is her only son)

In Jeremiah's day, Rachel weeps over her children once more, this time because they are being led into captivity (from Ramah) and exile near the very spot where she is buried. She is then comforted with the promise that her children will return. Once again, her offspring are both her cause of weeping and her hope for the future.

In Matthew's day, Rachel weeps yet again: this time over the slaughter of the children at Bethlehem. No words of comfort are given her in Matthew, but the very next verse speaks of Herod's death and the return of Joseph, Mary, and Jesus to the land of Israel. Just as in Jeremiah's day, the situation seems bleak, but the hope of salvation lives on.

# More reading #

See <https://www.accordancebible.com/Why-Is-Rachel-Weeping-At-Ramah>
See <https://www.accordancebible.com/Why-Is-Rachel-Weeping-At-Ramah-Part-2>
See <https://www.accordancebible.com/Why-Is-Rachel-Weeping-At-Ramah-Part-3>