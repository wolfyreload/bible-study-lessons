# Fruit of the Spirit VS gifts of the Spirit #

Lets start with not confusing the fruit of the Spirit with the gifts of the Spirit.

* "Fruit" of the Spirit comes 'naturally' as we are transformed into being more and more like Christ and this takes a lifetime.
* "Gifts" of the Spirit come all at once from the Holy Spirit, as He sees fit.

List of the fruit of the spirit (Galatians 5:22-23)

* Love
* Joy
* Peace
* Forbearance (patience/tolerance)
* Kindness
* Goodness
* Faithfulness
* Gentleness
* Self-control

List of spiritual gifts (Romans 12; 1 Corinthians 12–14 and Ephesians 4)

* Prophecy/Word of Wisdom/Word of Knowledge (Romans 12, 1 Corinthians 12, Ephesians 4)
* Serving/Ministering (Romans 12)
* Teaching/Word of Wisdom/Word of Knowledge (Romans 12, 1 Corinthians 12, Ephesians 4)
* Encouragement (Romans 12)
* Giving (Romans 12)
* Leadership/Pastors (Romans 12, Ephesians 4)
* Evangelists (Ephesians 4)
* Being Merciful (Romans 12)
* Supernatural Faith (1 Corinthians 12)
* Gifts of healing (1 Corinthians 12)
* Miraculous powers (1 Corinthians 12)
* Distinguishing between spirits (1 Corinthians 12)
* Speaking in tongues (1 Corinthians 12)
* Interpretation of tongues (1 Corinthians 12)

## Discussion ##

Not all of the spiritual gifts appear to be gifted anymore and many online sources say that these gifts were only given to the early church, namely:

* Speaking/interpreting tongues
* Miraculous powers
* Gifts of healing

Is it worth doing a "spiritual gift survey"?

* The problem with these surveys is that we are tempted to only serve God in the areas that we feel that we have been gifted.
* However, God will equip us with whichever gift or gifts is necessary to accomplish the task He has called us to do.
* If we are genuinely seeking God’s leading through prayer, fellowship and studying God's Word our gifts will more than likely become obvious.