# James Chapter 4 #

## Scripture NIV ##

### Submit Yourselves to God ###

4 What causes fights and quarrels among you? Don't they come from your desires that battle within you? 2 You desire but do not have, so you kill. You covet but you cannot get what you want, so you quarrel and fight. You do not have because you do not ask God. 3 When you ask, you do not receive, because you ask with wrong motives, that you may spend what you get on your pleasures.

4 You adulterous people,[a] don't you know that friendship with the world means enmity against God? Therefore, anyone who chooses to be a friend of the world becomes an enemy of God. 5 Or do you think Scripture says without reason that he jealously longs for the spirit he has caused to dwell in us[b]? 6 But he gives us more grace. That is why Scripture says:

"God opposes the proud
    but shows favor to the humble."[c]

7 Submit yourselves, then, to God. Resist the devil, and he will flee from you. 8 Come near to God and he will come near to you. Wash your hands, you sinners, and purify your hearts, you double-minded. 9 Grieve, mourn and wail. Change your laughter to mourning and your joy to gloom. 10 Humble yourselves before the Lord, and he will lift you up.

11 Brothers and sisters, do not slander one another. Anyone who speaks against a brother or sister[d] or judges them speaks against the law and judges it. When you judge the law, you are not keeping it, but sitting in judgment on it. 12 There is only one Lawgiver and Judge, the one who is able to save and destroy. But you—who are you to judge your neighbor?

### Boasting About Tomorrow ###

13 Now listen, you who say, "Today or tomorrow we will go to this or that city, spend a year there, carry on business and make money." 14 Why, you do not even know what will happen tomorrow. What is your life? You are a mist that appears for a little while and then vanishes. 15 Instead, you ought to say, "If it is the Lord's will, we will live and do this or that." 16 As it is, you boast in your arrogant schemes. All such boasting is evil. 17 If anyone, then, knows the good they ought to do and doesn't do it, it is sin for them.

## Scripture Amplified ##

### Things to Avoid ###

4 What leads to [the unending] [a]quarrels and conflicts among you? Do they not come from your [hedonistic] desires that wage war in your [bodily] members [fighting for control over you]? 2 You are jealous and covet [what others have] and [b]your lust goes unfulfilled; so you [c]murder. You are envious and cannot obtain [the object of your envy]; so you fight and battle. You do not have because you do not ask [it of God]. 3 You ask [God for something] and do not receive it, because you ask [d]with wrong motives [out of selfishness or with an unrighteous agenda], so that [when you get what you want] you may spend it on your [hedonistic] desires. 4 You adulteresses [disloyal sinners—flirting with the world and breaking your vow to God]! Do you not know that being the world's friend [that is, loving the things of the world] is being God's enemy? So whoever chooses to be a friend of the world makes himself an enemy of God. 5 Or do you think that the Scripture says to no purpose [e]that the [human] spirit which He has made to dwell in us lusts with envy? 6 But He gives us more and more grace [through the power of the Holy Spirit to defy sin and live an obedient life that reflects both our faith and our gratitude for our salvation]. Therefore, it says, "God is opposed to the proud and haughty, but [continually] gives [the gift of] grace to the humble [who turn away from self-righteousness]." 7 So submit to [the authority of] God. Resist the devil [stand firm against him] and he will flee from you. 8 Come close to God [with a contrite heart] and He will come close to you. Wash your hands, you sinners; and purify your [unfaithful] hearts, you double-minded [people]. 9 Be miserable and grieve and weep [over your sin]. Let your [foolish] laughter be turned to mourning and your [reckless] joy to gloom. 10 Humble yourselves [with an attitude of repentance and insignificance] in the presence of the Lord, and He will exalt you [He will lift you up, He will give you purpose].

11 Believers, do not speak against or slander one another. He who speaks [self-righteously] against a brother or [f]judges his brother [hypocritically], speaks against the Law and judges the Law. If you judge the Law, you are not a doer of the Law but a judge of it. 12 There is only one Lawgiver and Judge, the One who is able to save and to destroy [the one God who has the absolute power of life and death]; but who are you to [hypocritically or self-righteously] pass judgment on your neighbor?

13 Come now [and pay attention to this], you who say, "Today or tomorrow we will go to such and such a city, and spend a year there and carry on our business and make a profit." 14 [g]Yet you do not know [the least thing] [h]about what may happen in your life tomorrow. [What is secure in your life?] You are merely a vapor [like a puff of smoke or a wisp of steam from a cooking pot] that is visible for a little while and then vanishes [into thin air]. 15 Instead [i]you ought to say, "If the Lord wills, we will live and we will do this or that." 16 But as it is, you boast [vainly] in your pretension and arrogance. All such boasting is evil. 17 So any person who knows what is right to do but does not do it, to him it is sin.

## Scripture CEB ##

### Conflict with people and God ###

4 What is the source of conflict among you? What is the source of your disputes? Don't they come from your cravings that are at war in your own lives? 2 You long for something you don't have, so you commit murder. You are jealous for something you can't get, so you struggle and fight. You don't have because you don't ask. 3 You ask and don't have because you ask with evil intentions, to waste it on your own cravings.

4 You unfaithful people! Don't you know that friendship with the world means hostility toward God? So whoever wants to be the world's friend becomes God's enemy. 5 Or do you suppose that scripture is meaningless? Doesn't God long for our faithfulness in[a] the life he has given to us?[b] 6 But he gives us more grace. This is why it says, God stands against the proud, but favors the humble.[c] 7 Therefore, submit to God. Resist the devil, and he will run away from you. 8 Come near to God, and he will come near to you. Wash your hands, you sinners. Purify your hearts, you double-minded. 9 Cry out in sorrow, mourn, and weep! Let your laughter become mourning and your joy become sadness. 10 Humble yourselves before the Lord, and he will lift you up.

11 Brothers and sisters, don't say evil things about each other. Whoever insults or criticizes a brother or sister insults and criticizes the Law. If you find fault with the Law, you are not a doer of the Law but a judge over it. 12 There is only one lawgiver and judge, and he is able to save and to destroy. But you who judge your neighbor, who are you?

### Warning the proud and wealthy ###

13 Pay attention, you who say, "Today or tomorrow we will go to such-and-such a town. We will stay there a year, buying and selling, and making a profit." 14 You don't really know about tomorrow. What is your life? You are a mist that appears for only a short while before it vanishes. 15 Here's what you ought to say: "If the Lord wills, we will live and do this or that." 16 But now you boast and brag, and all such boasting is evil. 17 It is a sin when someone knows the right thing to do and doesn't do it.

## Study ##