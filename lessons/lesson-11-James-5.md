# James Chapter 5 #

## Scripture NIV ##

### Warning to Rich Oppressors ###

5 Now listen, you rich people, weep and wail because of the misery that is coming on you. 2 Your wealth has rotted, and moths have eaten your clothes. 3 Your gold and silver are corroded. Their corrosion will testify against you and eat your flesh like fire. You have hoarded wealth in the last days. 4 Look! The wages you failed to pay the workers who mowed your fields are crying out against you. The cries of the harvesters have reached the ears of the Lord Almighty. 5 You have lived on earth in luxury and self-indulgence. You have fattened yourselves in the day of slaughter.[a] 6 You have condemned and murdered the innocent one, who was not opposing you.

### Patience in Suffering ###

7 Be patient, then, brothers and sisters, until the Lord's coming. See how the farmer waits for the land to yield its valuable crop, patiently waiting for the autumn and spring rains. 8 You too, be patient and stand firm, because the Lord's coming is near. 9 Don't grumble against one another, brothers and sisters, or you will be judged. The Judge is standing at the door!

10 Brothers and sisters, as an example of patience in the face of suffering, take the prophets who spoke in the name of the Lord. 11 As you know, we count as blessed those who have persevered. You have heard of Job's perseverance and have seen what the Lord finally brought about. The Lord is full of compassion and mercy.

12 Above all, my brothers and sisters, do not swear—not by heaven or by earth or by anything else. All you need to say is a simple "Yes" or "No." Otherwise you will be condemned.

### The Prayer of Faith ###

13 Is anyone among you in trouble? Let them pray. Is anyone happy? Let them sing songs of praise. 14 Is anyone among you sick? Let them call the elders of the church to pray over them and anoint them with oil in the name of the Lord. 15 And the prayer offered in faith will make the sick person well; the Lord will raise them up. If they have sinned, they will be forgiven. 16 Therefore confess your sins to each other and pray for each other so that you may be healed. The prayer of a righteous person is powerful and effective.

17 Elijah was a human being, even as we are. He prayed earnestly that it would not rain, and it did not rain on the land for three and a half years. 18 Again he prayed, and the heavens gave rain, and the earth produced its crops.

19 My brothers and sisters, if one of you should wander from the truth and someone should bring that person back, 20 remember this: Whoever turns a sinner from the error of their way will save them from death and cover over a multitude of sins.

## Scripture Amplified ##

### Misuse of Riches ###

5 Come [quickly] now, you rich [who lack true faith and hoard and misuse your resources], weep and howl over the miseries [the woes, the judgments] that are coming upon you. 2 Your wealth has rotted and is ruined and your [fine] clothes have become moth-eaten. 3 Your gold and silver are corroded, and their corrosion will be a witness against you and will consume your flesh like fire. You have stored up your treasure in the last days [when it will do you no good]. 4 Look! The wages that you have [fraudulently] withheld from the laborers who have mowed your fields are crying out [against you for vengeance]; and the cries of the harvesters have come to the ears of [a]the Lord of Sabaoth. 5 On the earth you have lived luxuriously and abandoned yourselves to soft living and led a life of wanton pleasure [self-indulgence, self-gratification]; you have fattened your hearts in a day of slaughter. 6 You have condemned and have put to death the righteous man; he offers you no resistance.

### Exhortation ###

7 So wait patiently, brothers and sisters, until the coming of the Lord. The farmer waits [expectantly] for the precious harvest from the land, being patient about it, until it receives the early and late rains. 8 You too, be patient; strengthen your hearts [keep them energized and firmly committed to God], because the coming of the Lord is near. 9 Do not complain against one another, believers, so that you will not be judged [for it]. Look! The Judge is standing [b]right at the door. 10 As an example, brothers and sisters, of suffering and patience, take the prophets who spoke in the name of the Lord [as His messengers and representatives]. 11 You know we call those blessed [happy, spiritually prosperous, favored by God] who were steadfast and endured [difficult circumstances]. You have heard of the patient endurance of Job and you have seen the Lord's outcome [how He richly blessed Job]. The Lord is full of compassion and is merciful.

12 But above all, my fellow believers, do not swear, either by heaven or by earth or with any other oath; but let your yes be [a truthful] yes, and your no be [a truthful] no, so that you may not fall under judgment.

13 Is anyone among you suffering? He must pray. Is anyone joyful? He is to sing praises [to God]. 14 Is anyone among you sick? He must call for the elders (spiritual leaders) of the church and they are to pray over him, anointing him with [c]oil in the name of the Lord; 15 and the prayer of faith will restore the one who is sick, and the Lord will raise him up; and if he has committed sins, he will be forgiven. 16 Therefore, confess your sins to one another [your false steps, your offenses], and pray for one another, that you may be healed and restored. The heartfelt and persistent prayer of a righteous man (believer) can accomplish much [when put into action and made effective by God—it is dynamic and can have tremendous power]. 17 Elijah was a man with a nature like ours [with the same physical, mental, and spiritual limitations and shortcomings], and he prayed [d]intensely for it not to rain, and it did not rain on the earth for three years and six months. 18 Then he prayed again, and the sky gave rain and the land produced its crops [as usual].

19 My brothers and sisters, if anyone among you strays from the truth and falls into error and [another] one turns him back [to God], 20 let the [latter] one know that the one who has turned a sinner from the error of his way will save that one's soul from death and cover a multitude of sins [that is, obtain the pardon of the many sins committed by the one who has been restored].

## Scripture CEB ##

5 Pay attention, you wealthy people! Weep and moan over the miseries coming upon you. 2 Your riches have rotted. Moths have destroyed your clothes. 3 Your gold and silver have rusted, and their rust will be evidence against you. It will eat your flesh like fire. Consider the treasure you have hoarded in the last days. 4 Listen! Hear the cries of the wages of your field hands. These are the wages you stole from those who harvested your fields. The cries of the harvesters have reached the ears of the Lord of heavenly forces. 5 You have lived a self-satisfying life on this earth, a life of luxury. You have stuffed your hearts in preparation for the day of slaughter. 6 You have condemned and murdered the righteous one, who doesn't oppose you.

### Courageous patience ###

7 Therefore, brothers and sisters, you must be patient as you wait for the coming of the Lord. Consider the farmer who waits patiently for the coming of rain in the fall and spring, looking forward to the precious fruit of the earth. 8 You also must wait patiently, strengthening your resolve, because the coming of the Lord is near. 9 Don't complain about each other, brothers and sisters, so that you won't be judged. Look! The judge is standing at the door!

10 Brothers and sisters, take the prophets who spoke in the name of the Lord as an example of patient resolve and steadfastness. 11 Look at how we honor those who have practiced endurance. You have heard of the endurance of Job. And you have seen what the Lord has accomplished, for the Lord is full of compassion and mercy.

### Final instructions ###

12 Most important, my brothers and sisters, never make a solemn pledge—neither by heaven nor earth, nor by anything else. Instead, speak with a simple "Yes" or "No," or else you may fall under judgment.

13 If any of you are suffering, they should pray. If any of you are happy, they should sing. 14 If any of you are sick, they should call for the elders of the church, and the elders should pray over them, anointing them with oil in the name of the Lord. 15 Prayer that comes from faith will heal the sick, for the Lord will restore them to health. And if they have sinned, they will be forgiven. 16 For this reason, confess your sins to each other and pray for each other so that you may be healed. The prayer of the righteous person is powerful in what it can achieve. 17 Elijah was a person just like us. When he earnestly prayed that it wouldn't rain, no rain fell for three and a half years. 18 He prayed again, God sent rain, and the earth produced its fruit.

19 My brothers and sisters, if any of you wander from the truth and someone turns back the wanderer, 20 recognize that whoever brings a sinner back from the wrong path will save them from death and will bring about the forgiveness of many sins.

## Study ##