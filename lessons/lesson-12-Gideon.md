# Lesson 12 Gideon - Judges 6-8 #

* Israel did evil in the sight of the LORD
* God gave Israel over to the Midianites
* God's people cried out to the LORD for help
* God sends Gideon to save Israel

## Gideon asks for 3 confirmations ##

1. He first wants a sign from the angel of the LORD as a confirmation
2. Wet fleece and dry ground
3. Dry fleece and wet ground

## Gideon's tasks ##

1. Destroy the alter to Baal and the Asherah pole beside it. Replace those alter's with an alter to the LORD (this happens after confirmation 1).
2. Defeat the Midianites with 300 men (filtered down from 32,000) - to make sure that the LORD gets credit for saving Israel and not Israel

## Gideon's sin ##

Gideon creates an idol from the gold from Israel and Israel worshiped this idol

## Gideon side note ##

* Gideon is mentioned again in the new testament Hebrews 11:32

### Random calculation ###

* He has 71 sons, 70 from wives and 1 from a concubine
* Assuming that he had girls too he had around 142
* Assuming a period of 3.5 years between children (less chance having a child while weaning a child or while pregnant)
* Assuming a child bearing age of 16 to 45 years of age
* At approximately 8 children per wife
* Probably had 17-20 wives
