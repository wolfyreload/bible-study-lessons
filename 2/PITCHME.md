---?image=2/assets/background/first-page.jpg

# Names of the LORD #

![Tetragrammaton](2/assets/image/tetragrammaton.jpg)

---

## What is YHWH? What is the tetragrammaton? ##

* YHWH is the Hebrew name for our LORD God (in our alphabet) |
* The Old Testament is written in the ancient Hebrew language |
* Ancient Hebrew does not have vowels in its written form |
* The Jews used the name Adonai or "Lord" for fear of accidentally using God's name in vain. |

+++

## So how do we pronounce YHWH? ##

* Some versions of the Bible translate YHWH as "Jehovah" or "Yahweh" |
* Most English Bibles use the word "LORD" (all capital letters) |
* Regarding the correct pronunciation of God's name in Hebrew, we will only know in heaven one day. |

---

## Other Names of God ##

* Thoughtout the course of the Bible, God's followers have given the LORD multiple names |
* We going to look at some of these names and what they mean |

+++

## EL [el], ELOAH [el, el-oh-ah] and ELOHIM [el-oh-heem] ##

* Means God "Creator, Mighty and Strong" |
* Around 2600 occurances in the Bible |

+++

## EL SHADDAI [el-shah-dahy] ##

* God Almighty – speaks of God's ultimate power over all. |
* Genesis 49:24; Psalm 132:2,5 |

+++

## ADONAI [ah-daw-nahy] ##

* It means "Lord" (note not all caps). |
* Used in place of YHWH, which the Jews belived to be too sacred to be uttered by sinful men. |

+++

## YHWH / YAHWEH [yah-way] / JEHOVAH [ji-hoh-veh] ##

* Strictly speaking, this is the only proper name for God. |
* Translated in English Bibles as "LORD" (all capitals) to distinguish it from Adonai |

---

## YAHWEH-JIREH [yah-way-ji-reh] ##

* Means: "The LORD Will Provide" |
* First used by Abraham when God provided the ram to be sacrificed in place of Isaac (Genesis 22:14) |

+++

## YAHWEH-RAPHA [yah-way-raw-faw] ##

* Means: "The LORD Who Heals" |
* He heals in both body and soul. |
* In body, by healing us from physical diseases and injuries |
* In spirit/soul by pardoning our sins. |

+++

## YAHWEH-ROHI [yah-way-roh-hee] ##

* Means: "The LORD Is Our Shepherd " |
* First used by David when he was thinking about his relationship as a shepherd to his sheep |
* He realised that was exactly the relationship God has with him |
* So he said: "The LORD is my Shepherd. I shall not want" (Psalm 23:1) |

+++

## YAHWEH-ELOHIM [yah-way-el-oh-him] ##

* Means: "LORD God" and signifies that he is LORD of lords |

+++

## YAHWEH-SABAOTH [yah-way-sah-bah-ohth] ##

* Means: "The Lord Hosts" or "The LORD of the armies of heaven" |
* The name is expressive of the majesty, power, and authority of God |

+++

## EL-GIBHOR [el-ghee-bohr] ##

* Means: "Mighty God" |
* This is a prophetic description of the Messiah, our Lord Jesus Christ (Isaiah 9:6) |

+++

## EL-OLAM [el-oh-lahm] ##

* Means: "Everlasting God" |
* God is without beginning or end, free from all constraints of time |

---?image=assets/background/gitpitch-audience.jpg

### Conclusion ###

* Why does God have so many names? |
* In the Bible, a name is not just a label but it tells us something about the person |
* Each name of God describes a different aspect of God's character |
* What is your favorite name for the LORD? |